﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MikuecRenderingSetting{
    public void ChangeRendering(string renderingType){
        if (renderingType == "HighDefenition"){
            Debug.Log("hd");
            ToHdRendering();
        }else if(renderingType == "Standard"){
            Debug.Log("Standard");
            ToStanderdRendering();
        }else if (renderingType == "Toon")
        {
            Debug.Log("Toon");
            ToToonRendering();
        }else{
            throw new Exception("引数として \"Standard\", \"HighDefenition\" もしくは \"Toon\" を使用してください");
        }
    }

    private void ToHdRendering(){
        GameObject[] models = GameObject.FindGameObjectsWithTag("Mesh");
        foreach (GameObject model in models)
        {
            changeShader(model, "HDRenderPipeline/Lit");
        }
    }
    private void ToStanderdRendering(){
        GameObject[] models = GameObject.FindGameObjectsWithTag("Mesh");
        foreach (GameObject model in models)
        {
            changeShader(model, "Standard");
        }

    }
    private void ToToonRendering()
    {
        GameObject[] models = GameObject.FindGameObjectsWithTag("Mesh");
        foreach (GameObject model in models)
        {
            changeShader(model, "Toon/Basic");
        }

    }

    public static void changeShader(GameObject targetGameObject, string ShaderName_to, string ShaderName_from = "")
    {
        foreach (Transform t in targetGameObject.GetComponentsInChildren<Transform>(true)) //include inactive gameobjects
        {
            if (t.GetComponent<Renderer>() != null)
            {
                var materials = t.GetComponent<Renderer>().sharedMaterials;
                foreach (Material material in materials)
                {
                    if (ShaderName_from == "")
                    {
                        material.shader = Shader.Find(ShaderName_to);
                    }
                    else
                    {
                        if (material.shader.name == ShaderName_from)
                        {
                            material.shader = Shader.Find(ShaderName_to);
                        }
                    }
                }
            }
        }
    }
}
