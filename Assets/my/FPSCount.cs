﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCount : MonoBehaviour {

    int frameCount;
    float prevTime;
    public Text fpsText;

    private void Awake()
    {
        Application.targetFrameRate = 240; //60FPSに設定
    }

    void Start () {
        frameCount = 0;
        prevTime = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        ++frameCount;
        float time = Time.realtimeSinceStartup - prevTime;

        if (time >= 0.5f)
        {
            string fps = string.Format("{0:0.0}fps", frameCount / time);
            //Debug.Log(fps);
            fpsText.text = fps;

            frameCount = 0;
            prevTime = Time.realtimeSinceStartup;
        }
		
	}
}
