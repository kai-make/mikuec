﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour {
	public Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("1")){
			anim.Play("WAIT01",-1,0f);
		}
		else if (Input.GetKeyDown("2")){
			anim.Play("WAIT02",-1,0f);
		}
		else if (Input.GetKeyDown("3")){
			anim.Play("WAIT03",-1,0f);
		}
		else if (Input.GetKeyDown("4")){
			anim.Play("WAIT04",-1,0f);
		}
		else if (Input.GetKeyDown("5")){
			anim.Play("Take 2018-03-15 03_49_09 PM",-1,0f);
		}
		else if (Input.GetKeyDown("6")){
			anim.Play("Take 2018-03-15 04_32_32 PM",-1,0f);
		}
		else if (Input.GetKeyDown("0")){
			anim.Play("WAIT00",-1,0f);
		}
		
	}
}
