﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public enum OPTIONS
{
    Standard = 0,
    HD_Rendering = 1,
    Toon = 2
}

public class MikuecEditorGUI: EditorWindow {

    public OPTIONS op;

    [MenuItem("Mikuec/レンダリング")]
    static void Open()
    {
        GetWindow<MikuecEditorGUI>();
    }

    bool toggleValue;

    void OnGUI()
    {

        op = (OPTIONS)EditorGUILayout.EnumPopup("使用するシェーダー:", op);
        if (GUILayout.Button("適用"))
            InstantiatePrimitive(op);
    }


    void InstantiatePrimitive(OPTIONS op)
    {
        MikuecRenderingSetting renderingSetting = new MikuecRenderingSetting();
        switch (op)
        {
            case OPTIONS.Standard:
                renderingSetting.ChangeRendering("Standard");
                break;
            case OPTIONS.HD_Rendering:
                renderingSetting.ChangeRendering("HighDefenition");
                break;
            case OPTIONS.Toon:
                renderingSetting.ChangeRendering("Toon");
                break;
            default:
                Debug.LogError("Unrecognized Option");
                break;
        }
    }
}
