#!/bin/sh

if [ $# -ne 1 ]; then
    echo "\n    ### Enable or Disabe UEC proxy on Git ###"
    echo "    Usage: \"./uec_proxy.sh on\"    OR    \"./uec_proxy off\"\n"
    exit 1
fi

if [ $1 = 'on' ]; then
    git config --global http.proxy http://proxy.uec.ac.jp:8080
    git config --global https.proxy https://proxy.uec.ac.jp:8080
    echo "\n##Setted as following##"
    git config --global http.proxy
    git config --global https.proxy
    echo ""
    exit 0
fi

if [ $1 = 'off' ]; then
    git config --global --unset http.proxy
    git config --global --unset https.proxy
    echo "\n##Setted as following##"
    git config --global http.proxy
    git config --global https.proxy
    echo "(No output avove indicates no proxy setted)"
    echo ""
    exit 0
fi